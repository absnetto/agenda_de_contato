import 'dart:io';
import 'package:agenda_de_contato/helpers/contact_helper.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class ContactPage extends StatefulWidget {
  final Contact contact;

  ContactPage({this.contact});

  @override
  _ContactPageState createState() => _ContactPageState();
}

class _ContactPageState extends State<ContactPage> {
  ///Indica se o contato foi alterado
  bool _userEdited = false;

  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  final _phoneController = TextEditingController();

  final _nameFocus = FocusNode();

  Contact _editedContact;

  @override
  void initState() {
    super.initState();

    if (widget.contact == null) {
      _editedContact = Contact();
    } else {
      _editedContact = Contact.fromMap(widget.contact.toMap());

      _nameController.text = _editedContact.name;
      _emailController.text = _editedContact.email;
      _phoneController.text = _editedContact.phone;
    }
  } //initState

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.red,
          centerTitle: true,
          title: Text(_editedContact.name ?? "Novo Contato"),
        ), //AppBar

        ///Botão salvar
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            if (_editedContact.name != null && _editedContact.name.isNotEmpty) {
              Navigator.pop(context, _editedContact);
            } else {
              FocusScope.of(context).requestFocus(_nameFocus);
            }
          },
          backgroundColor: Colors.red,
          child: Icon(Icons.save),
        ), //FloatingActionButton
        body: SingleChildScrollView(
          padding: EdgeInsets.all(10.0),
          child: Column(
            children: <Widget>[
              GestureDetector(
                  child: Container(
                    height: 140.0,
                    width: 140.0,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                        image: _editedContact.img != null
                            ? FileImage(File(_editedContact.img))
                            : AssetImage("images/person.png"), //AssetImage
                        fit: BoxFit.cover,
                      ), //DecotationImage
                    ), //BoxDecoration
                  ), //Container
                  onTap: () {
                    ImagePicker.pickImage(source: ImageSource.camera)
                        .then((file) {
                      if (file == null) {
                        return;
                      } else {
                        setState(() {
                          _editedContact.img = file.path;
                        });
                      }
                    });
                  } //onTap,
                  ), //GestureDetector
              TextField(
                decoration: InputDecoration(labelText: "Nome"),
                controller: _nameController,
                focusNode: _nameFocus,
                onChanged: (texto) {
                  _userEdited = true;
                  setState(() {
                    _editedContact.name = texto;
                  });
                },
              ), //TextField
              TextField(
                decoration: InputDecoration(labelText: "Email"),
                controller: _emailController,
                keyboardType: TextInputType.emailAddress,
                onChanged: (texto) {
                  _userEdited = true;
                  _editedContact.email = texto;
                },
              ), //TextField
              TextField(
                decoration: InputDecoration(labelText: "Telefone"),
                controller: _phoneController,
                keyboardType: TextInputType.phone,
                onChanged: (texto) {
                  _userEdited = true;
                  _editedContact.phone = texto;
                },
              ), //TextField
            ], //Widget
          ), //Column
        ), //SingleChildScrollView
      ), //Scaffold
      onWillPop: _requestPop,
    ); //WillPopScope
  }

  Future<bool> _requestPop() {
    if (_userEdited) {
      showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Descartar alterações?"),
            content: Text("Se sair as alterações serão perdidas."),
            actions: <Widget>[
              FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text("Cancelar"),
              ),
              FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                  Navigator.pop(context);
                },
                child: Text("Sim"),
              ),
            ], //Widget
          ); //AlertDialog
        }, //builder
      ); //showDialog
      return Future.value(false);
    } else {
      return Future.value(true);
    }
  } //_requestPop
}
